# Squarish

A tiny tool to experiment with vector shapes. Try it at
[squarish.manufacturaindependente.org](http://squarish.manufacturaindependente.org).

If you're lost, click the `[i]` icon in the top right for how to use Squarish.

## Local storage

The Save option stores your drawings in your browser's local storage. If you
want to save them as a file, use the export PNG or SVG option.

## Colors

Colors are limited; it's easier to not implement a color picker right away, and
limited palettes are a great constraint for creativity.

Right now, we're including the [AYY4](https://lospec.com/palette-list/ayy4) and
[Japanese Woodblock](https://lospec.com/palette-list/japanese-woodblock)
palettes by [Polyducks](https://polyducks.co.uk).

## How to run locally

You only need Python installed in your system to run Squarish with one command:

    make serve

Then head to `http://localhost:8000` on your web browser.

## How to install on a hosted server

You can also put Squarish on a server and access it remotely. 

Just place the files in any web host and you're up -- there are no server-side
dependencies.

## Editing tiles

The tileset can easily be edited using [Inkscape](https://inkscape.org) to open
the `assets/tilesets/geo.svg` file. Keep in mind the following:

- Each tile must fit inside a single grid division, and it must be a single
  path. If your tile is made of more than one path, combine or add them.
- The order of tiles in the tile menu is defined by their order in the XML
  file, not their position. Change their file ordering using PgUp, PgDn, Home
  and End in Inkscape.
- Only shape fills are considered, strokes are removed. File an issue in case
  you'd like strokes to show and we'll look into it.

## Under the hood

Squarish has no backend: all the logic happens in the browser, including save
files. No data is collected, but an external web service is called to convert
the SVG data URI to a file for download (see `js/utils.js` for more details).
We hope to remove this dependency at some point.

Most of the heavy lifting is done by [Paper.js](https://paperjs.org), a
fantastic tool to turn vector graphics coding on the browser into an actual
delight.

The rest of the UI is done with the help of our old friend
[JQuery](https://jquery.org), along with the
[mousewheel](https://github.com/jquery/jquery-mousewheel/) and
[modal](https://jquerymodal.com/) plugins. Keyboard shortcuts are handled by
the elegant [Mousetrap](https://craig.is/killing/mice) library.

The geo tileset was designed by [Manufactura
Independente](https://manufacturaindependente.org) and is made available under
the [Creative Commons Attribution
Sharealike](https://creativecommons.org/licenses/by-sa/4.0/) license
(CC-BY-SA).

## Authors and license

Squarish is cooked up and implemented by [Manufactura
Independente](http://manufacturaindependente.org) (Ana Isabel Carvalho and
Ricardo Lafuente).

Squarish is free software, made available under the
[AGPL](https://www.gnu.org/licenses/agpl-3.0.html) license. This means that you
are free to use, modify, improve and redistribute the software, as long as you
share your improvements back under the same terms. Read the [full
license](https://www.gnu.org/licenses/agpl-3.0.html) for more details.

Any artwork you create with Squarish is your own to do with as you please.
