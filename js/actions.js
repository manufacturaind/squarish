/* Squarish v1.0
 *
 * Functions for all the actions and commands available in Squarish.
 *
 * Copyright 2020 Manufactura Independente (Ana Isabel Carvalho and Ricardo Lafuente)
 *
 * Distributed under the terms of the AGPL. See the LICENSE file or visit
 * https://www.gnu.org/licenses/agpl-3.0.en.html.
 *
 */


function loadTileset(url) {
  // Load a set of tiles from a properly formatted SVG file
  project.importSVG(url, {
    insert: false,
    onLoad: function(item, svg) {
      // Place tileset in the tiles var
      tiles = item.children.layer1.children;
      // Before anything, translate all paths in the SVG to have the same origin point
      for (var t in tiles) {
        var tile = tiles[t];
        while (tile.bounds.x >= 99) {
          // The 99 value should be 100, but this gives a little leeway to some
          // SVG coordinate rounding that happens here and there
          tile.bounds.x -= 100;
        }
        while (tile.bounds.y >= 99) {
          tile.bounds.y -= 100;
        }
      }
      applyTileset();

      // set up inverted tiles too
      invertedTiles = [];
      tiles.forEach( function(t) {
        var square = new Path.Rectangle({
          point: new Point(0,0), 
          size: new Size(100, 100),
          insert: false
        });
        if (t.type == 'rectangle' && t.size.width == 100 && t.size.height == 100) {
          // full rectangle draws an error when subtracting, so just don't alter it
          invertedTiles.push(t);
        } else {
          // create new path by subtracting the original tile from the rectangle
          var newPath = square.subtract(t);
          invertedTiles.push(newPath);
        }
      });
    }
  });
}

function applyTileset(keepSelection) {
  // Get the currently loaded tileset (stored in the tiles var) and update the UI

  // clear the tilebar
  $('#tilebar').children().remove();
  // Place copies of the tiles in the tilebar
  var counter = 0;
  tiles.forEach( function(t) {
    // Convert tile to SVG node, since the tilebar is not managed by Paper.js
    var tile = t.clone({ insert: false });
    tile.scale(0.5, new Point(50, 50));
    tile.translate(-25, -25);
    var svgTile = tile.exportSVG();
    svgTile.fillColor = activeColor;
    svgTile['data-tile-number'] = counter;
    $('#tilebar').append(
      // 50px on the viewbox always!
      $('<svg viewBox="0 0 50 50" height="40" width="40">')
        .attr('data-tile-number', counter)
        .append($(svgTile))
    );
    counter++;
  });

  $('#tilebar svg').children()
    .attr('fill', '#b5bae5')
    .removeClass('active');
  // Set first tile as active
  if (!keepSelection) {
    $('#tilebar svg').first().addClass('active');
    $('#tilebar svg').first().children().attr('fill', activeColor);
  } else {
    $('#tilebar svg').eq(activeTile).addClass('active');
    $('#tilebar svg').eq(activeTile).children().attr('fill', activeColor);

  }

  $("#tilebar svg").click(function(event) {
    // Select new tile: first remove active status from all tiles
    $('#tilebar svg').removeClass('active');
    $('#tilebar svg').children().attr('fill', '#b5bae5');

    activeTile = $(this).attr('data-tile-number');
    $(this).children().attr('fill', activeColor);
    $(this).addClass('active');
  });
  $("#tilebar svg").hover(
    function() { // mouseEnter
      $(this).children().attr('fill', activeColor);
    },
    function() { // mouseLeave
      if (activeTile != $(this).attr('data-tile-number') ) {
        /*$(this).css('background', 'white');*/
        $(this).children().attr('fill', '#b5bae5');
      }
    }
  );

  if (rotation) {
    $('#tilebar').children('svg').children().attr('transform',
      "translate(25,25)\nrotate(" + rotation + ")\ntranslate(-25,-25)"
    );
    if (previewPath) {
      x = 100 * Math.floor(currentPreviewPoint.x/100);
      y = 100 * Math.floor(currentPreviewPoint.y/100);
      previewPath.rotate(rotation, new Point(x+50, y+50));
    }
  }
}

function invertTiles() {
  var tmp = tiles;
  tiles = invertedTiles;
  invertedTiles = tmp;
  applyTileset(true);
  drawTilePreview();
}

function toggleGrid() {
  gridLayer.visible = !gridLayer.visible;
}

function toggleToolbar() {
  // toggle element visibility
  var toolbar = document.getElementById('tools');
  var togglebutton = document.getElementById('toggle-tools');
  var floatactions = document.getElementById('floating-actions');
  // toolbar.style.display = toolbar.style.display == "none" ? "block" : "none";
  toolbar.onclick = toolbar.classList.toggle('is-hidden');
  togglebutton.onclick = togglebutton.classList.toggle('is-hidden');
  floatactions.onclick = floatactions.classList.toggle('is-far-right');
  view.draw();
}

function zoomIn(offset) {
  if (!offset) { offset = 1.5; }
  if (view.zoom < 3) {
    view.zoom = view.zoom * offset;
  }
  updateBackground();
}

function zoomOut(offset) {
  if (!offset) { offset = 1.5; }
  if (view.zoom > 0.1) {
    view.zoom = view.zoom / offset;
  }
  updateBackground();
}

function moveCanvas(direction, extra) {
  // for moving with arrow keys
  var moveOffset = 50;
  if (extra) {
    moveOffset = 250;
  }
  if (direction == 'up') {
    view.center = new Point(view.center.x, view.center.y-moveOffset);
  } else if (direction == 'down') {
    view.center = new Point(view.center.x, view.center.y+moveOffset);
  } else if (direction == 'left') {
    view.center = new Point(view.center.x-moveOffset, view.center.y);
  } else if (direction == 'right') {
    view.center = new Point(view.center.x+moveOffset, view.center.y);
  }
  updateBackground();
}

function resetZoom() {
  view.zoom = 1;
  updateBackground();
}

function rotateTiles(ccw) {
  // Add 90º to the global rotation setting. If `ccw` is true, subtract 90º instead.
  if (ccw) {
    if (rotation <= 0) {
      rotation = 270;
    } else {
      rotation -= 90;
    }
  } else {
    if (rotation >= 270) {
      rotation = 0;
    } else {
      rotation += 90;
    }

  }
  $('#tilebar').children('svg').children().attr('transform',
    "translate(25,25)\nrotate(" + rotation + ")\ntranslate(-25,-25)"
  );
  if (previewPath) {
    var previewRota = 90;
    if (ccw) {
      previewRota = -90;
    }
    x = 100 * Math.floor(currentPreviewPoint.x/100);
    y = 100 * Math.floor(currentPreviewPoint.y/100);
    previewPath.rotate(previewRota, new Point(x+50, y+50));
  }
}

function drawTile(point, behind) {
  // Draw the selected tile with the selected color on the cell that's being hovered on.
  // If `behind` is true, it will be placed under existing paths.
  // First round coordinates to fit the grid
  x = 100 * Math.floor(point.x/100);
  y = 100 * Math.floor(point.y/100);
  var newPath = tiles[activeTile].clone();
  if (rotation) {
    newPath.rotate(rotation, new Point(50, 50));
  }
  newPath.translate(x,y);
  newPath.fillColor = activeColor;
  mainLayer.addChild(newPath);
  if (behind) {
    newPath.sendToBack();
    undoHistory.push(['place-bottom', newPath]);
  } else {
    undoHistory.push(['place-top', newPath]);
  }
  redoHistory = [];
  // debug helper to get drawing bounds
  // debugLayer.removeChildren();
  // debugLayer.activate()
  // var boundsRect = new Path.Rectangle(mainLayer.bounds);
  // boundsRect.strokeColor = '#cc2244';
  mainLayer.activate();
}

function drawTilePreview(point) {
  if (!point) {
    // no point = redraw preview
    point = currentPreviewPoint;
    currentPreviewPoint = new Point(0, 0);
  }
  // Draw a "ghost" path representing what would be placed if the user clicks the cell.
  // First round coordinates to fit the grid
  x = 100 * Math.floor(point.x/100);
  y = 100 * Math.floor(point.y/100);
  // Only draw if a different cell was hovered on
  if (currentPreviewPoint.x != x || currentPreviewPoint.y != y) {
    previewLayer.removeChildren();
    currentPreviewPoint = new Point(x, y);
    if (tiles != undefined) {
      // avoid error on startup
      previewPath = tiles[activeTile].clone();
      if (rotation) {
        previewPath.rotate(rotation, new Point(50, 50));
      }
      previewPath.translate(x,y);
      previewPath.fillColor = activeColor;
      previewPath.opacity = 0.2;
      previewLayer.addChild(previewPath);
    }
  }
}

function deleteTile(point, behind) {
  // Deletes the tile at `point`. If `behind` is true and there's more than one tile
  // in the clicked cell, it deletes the one on the bottom; otherwise the topmost tile
  // is deleted.
  var areaRect = new Rectangle(
    new Point(100 * Math.floor(point.x/100) - 2, 100 * Math.floor(point.y/100) - 2),
    new Point(100 * Math.ceil(point.x/100) + 2, 100 * Math.ceil(point.y/100) + 2)
  );
  var foundTiles = mainLayer.getItems({ inside: areaRect });
  var path;
  if (foundTiles.length) {
    if (behind) {
      path = foundTiles[0];
      path.remove();
      undoHistory.push(['remove-bottom', path]);
    } else {
      path = foundTiles[foundTiles.length-1];
      path.remove();
      undoHistory.push(['remove-bottom', path]);
    }
  }
  redoHistory = [];
}

function undoAction() {
  var action = undoHistory[undoHistory.length-1][0];
  var path = undoHistory[undoHistory.length-1][1];

  if (action == 'place-top' || action == 'place-bottom') {
    path.remove();
  } else if (action == 'remove-top') {
    mainLayer.addChild(path);
  } else if (action == 'remove-bottom') {
    mainLayer.addChild(path);
    path.sendToBack();
  } else if (action == 'set-background') {
    // FIXME: clarify variable name, in this case it's not a path
    setBackground(path[1], true);
  } 
  // remove last item of undo history and add it to redo history
  redoHistory.push(undoHistory.pop());
}

function redoAction() {
  var action = redoHistory[redoHistory.length-1][0];
  var path = redoHistory[redoHistory.length-1][1];

  if (action == 'remove-top' || action == 'remove-bottom') {
    path.remove();
  } else if (action == 'place-top') {
    mainLayer.addChild(path);
  } else if (action == 'place-bottom') {
    mainLayer.addChild(path);
    path.sendToBack();
  } else if (action == 'set-background') {
    // FIXME: clarify variable name, in this case it's not a path
    setBackground(path[0], true);
  }
  // remove last item of redo history and add it to undo history
  undoHistory.push(redoHistory.pop());
}


function showHelp() {
  $("#help-modal").modal();
}

function showLoadMenu() {
  // Get the list of drawings and show a modal for the user to pick one to load.
  var drawings = getSavedDrawings();
  for (var i in drawings) {
    var drawing = drawings[i];
    var baseName = drawing.replace('save-', '');
    var thumbData = localStorage.getItem('thumb-' + baseName);
    // We populate the load menu with the saved files when we first open it,
    // but we keep those referenced in the `savedDrawings` var to avoid re-appending them
    if (savedDrawings.indexOf(drawing) == -1) {
      // Only append if this drawing isn't already in the menu
      $("#drawing-list").append(
        $('<li class="load-item">').append(
          $('<a href="#" data-drawing="' + drawing + '">').append(
            '<img class="thumbnail" src="' + thumbData + '" data-drawing="' + drawing + '">',
          ),
          $('<span class="item-actions">').append(
            $('<a class="button no-bg" class="item-name" href="#" data-drawing="' + drawing + '">' + baseName + '</a>'),
            $('<a class="button" href="#" onclick="deleteDrawing(\'' + drawing + '\');""><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a>'),
            ),
        )
      );
      savedDrawings.push(drawing);
    }
  }
  $("#drawing-list .load-item a").click( function() {
    loadDrawing($(this).attr('data-drawing'));
  });
  $("#load-modal").modal();
}

function loadDrawing(name) {
  // Loads the drawing with key `name` from local storage.
  // First remove current project's contents
  project.layers.main.remove();
  project.layers.debug.remove();
  project.layers.background.remove();

  var savedData = localStorage.getItem(name);
  project.importJSON(savedData);
  projectName = name.split(":")[0].replace('save-', '');
  // Update the reference to the main layer
  if (project.layers[project.layers.length-1].getChildren() == 0) {
    project.layers.pop();
  }
  mainLayer = project.layers[project.layers.length-1];
  $.modal.close();
}
function loadLastDrawing() {
  // Loads the last saved drawing.
  var drawings = Object.keys(localStorage).sort();
  // remove a strange webpack item in localStorage
  var index = drawings.indexOf('loglevel:webpack-dev-server');
  if (index > -1) {
      drawings.splice(index, 1);
  }
  var lastDrawingName = drawings[drawings.length-1];
  loadDrawing(name);
}

function saveAction() {
  // Saves the project's JSON into a localStorage entry.
  // var name = projectName + ': ' + new Date().toString().split(" ").slice(0,5).join(" ");
  var name = 'save-' + projectName;
  // Remove grid and preview path
  removeHelperItems();
  // Save drawing
  localStorage.setItem(name, project.exportJSON());
  console.log('Saved as: ' + name);
  // Generate thumb
  createThumbnail(projectName);
  setTimeout(function() {
    restoreHelperItems();
  }, 100);
}

function saveDrawing(saveas) {
  // Saves the drawing in local storage. If it's called for the first time, a modal will show
  // to ask the user for the project's name. If `saveas` is true, the user will be asked for
  // a new name.
  if (!projectName || saveas) {
    $('a#set-project-name').click(function(event) {
      var name = $('#project-name').val();
      if (name) {
        projectName = name;
        saveAction();
        $.modal.close();
      }
    });
    $('#project-name').on('keyup', function (event) {
      if (event.keyCode === 13) {
        var name = $('#project-name').val();
        if (name) {
          projectName = name;
          saveAction();
          $.modal.close();
        }
      }
    });
    $("#project-name-modal").modal();
    // clear input box
    $('#project-name').val('');
    $('#project-name').focus();
  } else {
    saveAction();
  }
}

function deleteDrawing(name) {
  $('#load-modal [data-drawing="' + name + '"]').remove();
  localStorage.removeItem(name);
}

function exportSVG() {
  // Creates an SVG representation of the drawing and shows a browser download menu.
  var filename = 'squarish-export.svg';
  if (projectName) {
    filename = projectName + '.svg';
  }
  removeHelperItems();
  var svg = project.exportSVG({
    asString: true,
    matchShapes: true,
    bounds: mainLayer.bounds
  });
  saveAs(new Blob([svg], {type:"application/svg+xml"}), filename);
  restoreHelperItems();
}

function exportPNG() {
  // Creates a PNG representation of the drawing and shows a browser download menu.
  var filename = 'squarish-export.png';
  if (projectName) {
    filename = projectName + '.png';
  }
  previewPath.remove();
  gridItems = gridLayer.removeChildren();
  setTimeout(function() {
    // We have to use a timeout since the canvas snapshot (with toDataURL()) fires
    // before paper.js has time to do the grid hiding
    var imageData = document.getElementById('squarish').toDataURL();
    saveAs(imageData, filename);
    gridLayer.addChildren(gridItems);
  }, 50);
}

function setPalette(index) {
  // Sets the active palette (from a numeric index) and updates the UI
  palette = palettes[index];
  $('#palette-title').html(palette.name);
  // Flush previous palette
  $('.swatch').remove();
  $('ul#swatches').remove();
  // Now create swatches for each of the palette's colors
  $swatchesHolder = $('<ul id="swatches">');
  $("#colorbar").append($swatchesHolder);
  palette.colors.forEach(function(value) {
    $swatchesHolder.append(
      $('<li class="swatch">')
      .css('background', value)
      .attr('data-value', value)
      .attr('oncontextmenu','return false;' // disable right-click context menu
      )
    );
  });
  $('.swatch').mousedown( function(event) {
    var color = event.target.dataset.value;
    if (event.which == 1) {
      // Left-click: Set active tile color
      activeColor = color;
      $('#tilebar svg.active').children().attr('fill', color);
    } else if (event.which == 3) {
      // Right-click: Set background color
      setBackground(color);
    }
  });
}

function setBackground(color, skipHistory) {
  var previousColor;
  if (backgroundRect) {
    previousColor = backgroundRect.fillColor;
  }
  if (!skipHistory) {
    undoHistory.push(['set-background', [color, previousColor]]);
  }
  if (!color) {
    if (backgroundRect) {
      backgroundRect.remove();
    }
  } else {
    project.layers.background.activate();
    if (backgroundRect) {
      backgroundRect.remove();
    }
    backgroundRect = new Path.Rectangle({
      point: [view.bounds.x, view.bounds.y],
      fillColor: color,
      size: view.bounds,
    });
    project.layers.main.activate();
  }
}

function removeBackground() {
  if (backgroundRect) {
    backgroundRect.remove();
  }
}

function updateBackground() {
  if (backgroundRect) {
    project.layers.background.activate();
    color = backgroundRect.fillColor;
    backgroundRect.remove();
    backgroundRect = new Path.Rectangle({
      point: [view.bounds.x, view.bounds.y],
      size: view.bounds,
      fillColor: color
    });
    project.layers.main.activate();
  }
}

function fitBackgroundToDrawing() {
  project.layers.background.activate();
  color = backgroundRect.fillColor;
  backgroundRect.remove();
  backgroundRect = new Path.Rectangle({
    point: [project.layers.main.bounds.x, project.layers.main.bounds.y],
    size: project.layers.main.bounds,
    fillColor: color
  });
  project.layers.main.activate();
  return backgroundRect.bounds;
}

function prevPalette() {
  if (palettes.indexOf(palette) === 0) {
    setPalette(palettes.length - 1);
  } else {
    setPalette(palettes.indexOf(palette) - 1);
  }
}
function nextPalette() {
  if (palettes.indexOf(palette) > palettes.length - 2) {
    setPalette(0);
  } else {
    setPalette(palettes.indexOf(palette) + 1);
  }
}
