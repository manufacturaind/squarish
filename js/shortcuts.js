
Mousetrap.bind('r', function() { rotateTiles(); });
Mousetrap.bind('shift+r', function() { rotateTiles(true); }); // Rotate counterclockwise
Mousetrap.bind('x', function() { invertTiles(); });

Mousetrap.bind('g', function() { toggleGrid(); });
Mousetrap.bind('tab', function(e) { e.preventDefault(); toggleToolbar(); });

Mousetrap.bind('shift+l', function() { loadLastDrawing(); });
Mousetrap.bind('l', function() { showLoadMenu(); });
Mousetrap.bind(['s', 'ctrl+s'], function() { saveDrawing(); });
Mousetrap.bind(['shift+s', 'ctrl+shift+s'], function() { saveDrawing(true); }); // Save as
Mousetrap.bind('e', function() { exportPNG(); });

Mousetrap.bind('ctrl+z', function() { undoAction(); });
Mousetrap.bind('ctrl+shift+z', function() { redoAction(); });

Mousetrap.bind('+', function() { zoomIn(); });
Mousetrap.bind('-', function() { zoomOut(); });
Mousetrap.bind('1', function() { resetZoom(); });

Mousetrap.bind('up', function() { moveCanvas('up'); });
Mousetrap.bind('down', function() { moveCanvas('down'); });
Mousetrap.bind('left', function() { moveCanvas('left'); });
Mousetrap.bind('right', function() { moveCanvas('right'); });
Mousetrap.bind('shift+up', function() { moveCanvas('up', true); }); // Longer move
Mousetrap.bind('shift+down', function() { moveCanvas('down', true); });
Mousetrap.bind('shift+left', function() { moveCanvas('left', true); });
Mousetrap.bind('shift+right', function() { moveCanvas('right', true); });

Mousetrap.bind('?', function() { showHelp(); });

