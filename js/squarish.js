/* Squarish v1.0
 *
 * This file takes care of initializing Paper.js and the base Squarish elements.
 *
 * Copyright 2020 Manufactura Independente (Ana Isabel Carvalho and Ricardo Lafuente)
 *
 * Distributed under the terms of the AGPL. See the LICENSE file or visit
 * https://www.gnu.org/licenses/agpl-3.0.en.html.
 *
 */

var activeTile = 0; // Index of the currently selected tile
var palette = palettes[0]; // Active palette
var activeColor = palette.colors[0];
var rotation = 0; // Rotation to apply in degrees

var tiles; // Cache object for storing the tile pathreferences
var invertedTiles; // Cache object for storing the tile pathreferences

var mainLayer;     // Layer containing the drawing items
var gridLayer;     // Layer containing the grid
var previewLayer;  // Layer containing the path preview that shows when the cursor hovers over the canvas
var debugLayer;    // Layer for placing items to help debug issues
var previewPath;   // The preview path that's shown on hover
var backgroundRect; // Rectangle to be used as a color background
var savedDrawings = [];  // List of the saved drawings
var currentPreviewPoint; // Origin of the canvas grid cell that is being hovered on
var projectName;
var undoHistory = [];
var redoHistory = [];
var gridItems = [];
var debugItems = [];

// Install Paper.js globally
paper.install(window);
window.onload = function() {
  // Init Paper.js
  paper.setup('squarish');
  mainLayer = project.activeLayer;
  mainLayer.name = 'main';
  debugLayer = new Layer({ name: 'debug' });
  debugLayer.sendToBack();

  // Load tileset SVG and update the UI
  loadTileset('assets/tilesets/geo.svg');
  // Set up grid, background and debug layers
  gridLayer = new Layer({ name: 'grid' });
  drawLineGrid(50, 50, view.bounds);
  gridLayer.sendToBack();
  backgroundLayer = new Layer({ name: 'background' });
  backgroundLayer.sendToBack();

  // Init reference point for tile preview
  previewLayer = new Layer({ name: 'preview' });
  currentPreviewPoint = new Point(0, 0);
  // Set main layer as active again
  mainLayer.activate();
  // Populate color UI
  setPalette(0);

  // Clicks on canvas
  view.onMouseDown = function(event) {
    var button = event.event.which;
    if (button == 1) {
      if (!event.event.altKey) {
        // Left click: Draw active tile on cursor position
        drawTile(event.point);
      } else {
        // Alt-Left click: Draw active tile under existing tiles on cursor position
        drawTile(event.point, true);
      }
    } else if (button == 3) {
      if (!event.event.altKey) {
        // Right click: Delete topmost tile under cursor
        deleteTile(event.point);
      } else {
        // Alt-Right click: Delete bottom-most tile under cursor
        deleteTile(event.point, true);
      }
    } 
  };

  view.onMouseMove = function(event) {
    if (event.event.shiftKey) {
      // mousemove with shift pressed: pan view
      var factor = 0.7;
      view.center = new Point(view.center.x + factor * -event.delta.x,
                              view.center.y + factor * -event.delta.y);
      updateBackground();
    } else {
      drawTilePreview(event.point);
    }
  };

  // Mousewheel on canvas
  $('#squarish').mousewheel(function(event) {
    if (event.shiftKey) {
      // shift + mousewheel: zoom
      if (event.deltaY > 0) {
        zoomIn(1.05);
      } else if (event.deltaY < 0) {
        zoomOut(1.05);
      }
    } else {
      // mousewheel: pan view
      var factor = 15;
      view.center = new Point(view.center.x + factor * -event.deltaX,
                              view.center.y + factor * event.deltaY);
      updateBackground();
    }
  });

  view.onMouseDrag = function(event) {
    // pan with middle-click + drag (best for an actual mouse)
    if (event.event.buttons == 4) {
      var factor = 0.7;
      view.center = new Point(view.center.x + factor * event.delta.x,
                              view.center.y + factor * event.delta.y);
      updateBackground();
    }
  };
};
 
window.onbeforeunload = function() {
  return "Data will be lost if you leave the page, are you sure?";
};
