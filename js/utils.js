/* 
 * Utility and helper functions, to be called mostly from actions.js
 *
 * Copyright 2020 Manufactura Independente (Ana Isabel Carvalho and Ricardo Lafuente)
 *
 * Distributed under the terms of the AGPL. See the LICENSE file or visit
 * https://www.gnu.org/licenses/agpl-3.0.en.html.
 */

function getSavedDrawings() {
  // Returns a list of the existing drawings in the browser local storage.
  var drawings = Object.keys(localStorage).sort().filter(name => name.startsWith('save-'));
  // remove a strange webpack item in localStorage
  var index = drawings.indexOf('loglevel:webpack-dev-server');
  if (index > -1) {
      drawings.splice(index, 1);
  }
  return drawings;
}

function drawLineGrid(num_rectangles_wide, num_rectangles_tall, boundingRect) {
  // adapted from https://stackoverflow.com/a/12085314
  var width_per_rectangle = 100;
  var height_per_rectangle = 100;
  var gridLine;
  for (var i = 0; i <= num_rectangles_wide; i++) {
    var xPos = boundingRect.left + i * width_per_rectangle;
    var topPoint = new Point(xPos, boundingRect.top);
    var bottomPoint = new Point(xPos, num_rectangles_tall * height_per_rectangle);
    gridLine = new Path.Line(topPoint, bottomPoint);
    gridLine.strokeColor = 'grey';
    gridLine.dashArray = [3, 4];
  }
  for (var j = 0; j <= num_rectangles_tall; j++) {
    var yPos = boundingRect.top + j * height_per_rectangle;
    var leftPoint = new Point(boundingRect.left, yPos);
    var rightPoint = new Point(num_rectangles_wide * width_per_rectangle, yPos);
    gridLine = new Path.Line(leftPoint, rightPoint);
    gridLine.strokeColor = 'grey';
    gridLine.dashArray = [3, 4];
  }
}

function drawDotGrid(num_rectangles_wide, num_rectangles_tall, boundingRect) {
  // adapted from https://stackoverflow.com/a/12085314
  var width_per_rectangle = 100;
  var height_per_rectangle = 100;
  for (var i = 0; i <= num_rectangles_wide; i++) {
    var xPos = boundingRect.left + i * width_per_rectangle;
    for (var j = 0; j <= num_rectangles_tall; j++) {
      var yPos = boundingRect.top + j * height_per_rectangle;
      var gridPoint = new Path.Circle(new Point(xPos, yPos), 3);
      gridPoint.fillColor = 'grey';
    }
  }
  console.log(gridLayer.children);
}

function getPNGImageData() {
  // https://stackoverflow.com/a/11112370
  return document.getElementById('squarish').toDataURL('image/png');
}

function createThumbnail(name) {
  // Generates a thumbnail of the current drawing and saves it in localStorage in the format `thumb-name`.
  // Thumbs have a width of 300px
  
  removeHelperItems();
  setTimeout(function() {
    var pngData = getPNGImageData();
    img.src = pngData;
  }, 100);
  // https://stackoverflow.com/a/43809873/122400
  var canvas = document.createElement('canvas');
  var context = canvas.getContext('2d');
  var maxW = 300;
  var maxH = 500;
  var img = document.createElement('img');

  img.onload = function() {
    // First, crop to the bounds of mainLayer
    var croppedURL = cropToBounds(img);
    var cropImg = new Image();
    cropImg.src = croppedURL;

    // Then resize it down to thumb size (width of 300px)
    var boundsRect = new Path.Rectangle(mainLayer.bounds);
    var iw = boundsRect.bounds.width;
    var ih = boundsRect.bounds.height;
    var scale = Math.min((maxW / iw), (maxH / ih));
    var iwScaled = iw * scale;
    var ihScaled = ih * scale;
    canvas.width = iwScaled;
    canvas.height = ihScaled;
    // FIXME: not resizing properly, I get a blank result, why?
    context.drawImage(cropImg, 0, 0, iwScaled, ihScaled);
    var thumbnail = canvas.toDataURL();
    // FIXME: change this to `thumbnail` when the above is fixed
    localStorage.setItem('thumb-' + name, cropImg.src);
  };
}

function cropToBounds(img){
  // Return a cropped PNG version of the canvas, using the bounds from mainLayer
  // https://stackoverflow.com/a/34244421/122400
  var canvas1 = document.createElement('canvas');
  var ctx1 = canvas1.getContext('2d');
  // shortcut for readability
  var boundsRect = new Path.Rectangle(mainLayer.bounds);
  var bs = boundsRect.bounds;
  canvas1.width = bs.width;
  canvas1.height = bs.height;
  ctx1.drawImage(img, bs.topLeft.x, bs.topRight.y, bs.width, bs.height, 0, 0, bs.width, bs.height);
  return(canvas1.toDataURL());
}

function removeHelperItems() {
  // Hides grid and path preview, to be used before exporting the project
  previewPath.remove();
  if (gridLayer.children.length) {
    gridItems = gridLayer.removeChildren();
  }
  if (debugLayer.children.length) {
    debugItems = debugLayer.removeChildren();
  }
}

function restoreHelperItems() {
  // Restores the items removed with removeHelperItems();
  gridLayer.addChildren(gridItems);
  debugLayer.addChildren(debugItems);
}
